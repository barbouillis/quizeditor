package ;

import haxe.io.Path;
import haxe.ui.ComponentBuilder;
import haxe.ui.containers.HBox;
import haxe.ui.containers.VBox;
import haxe.ui.components.Button;
import haxe.ui.HaxeUIApp;

class Main {

    public static var mainContainer:VBox = null;

    public static var openedPath:String = null;
    public static function main() {

        #if sys
        var filenames = Sys.args();

        // for now open only one file
        var filename = filenames.shift();
        if (filename != null) {
            if (Path.extension(filename) == "gift") {
                openedPath = filename;
            }
        }
        #end
        
        var app = new HaxeUIApp();
        app.ready(function() {

            HaxeUIApp.instance.icon = "images/quiz.png";

            var mainEntry = new MainEntry();
            mainEntry.percentWidth = 100;
            mainEntry.percentHeight = 100;

            mainContainer = mainEntry.findComponent("main_container");
            #if  haxeui_hxwidgets
            var systemLanguage = hx.widgets.Locale.systemLanguage;
            var lang  = hx.widgets.Locale.getLanguageCanonicalName(systemLanguage);
            haxe.ui.locale.LocaleManager.instance.language = lang;
            #end

            app.addComponent(mainEntry);



            app.start();
        });
    }
}
