package ;

import haxe.ui.containers.VBox;
import haxe.ui.containers.menus.Menu;
import haxe.ui.events.MouseEvent;
import haxe.ui.events.UIEvent;
import haxe.ui.containers.ButtonBar;
import haxe.ui.components.Button;


@:build(haxe.ui.ComponentBuilder.build("assets/main-app.xml"))
class MainEntry extends VBox {

    @:bind(this, UIEvent.INITIALIZE)
    private function onStart(e) {

        #if haxeui_hxwidgets 
        menu.show();
        #end
        buttonbar.show();
    }

    #if haxeui_hxwidgets 

    @:bind(menu, MenuEvent.MENU_SELECTED)
    private function onSelectMenu(e:MenuEvent) {
        switch (e.menuItem.id) {
            case "quit":
                #if sys Sys.exit(0); #end
            case "open_player":
                var player = Main.mainContainer.findComponent(MainPlayer);
                if (player != null) return;
                var editor = Main.mainContainer.findComponent(MainEditor);
                if (editor != null) editor.hide(); //removeComponent(editor);
                for( c in Main.mainContainer.childComponents) {
                    if ((c is MainEditor)) continue;
                    c.parentComponent.removeComponent(c);
                }
                Main.mainContainer.addComponent(new MainPlayer());
                buttonbar.hide();
            case "open_editor":
                var player = Main.mainContainer.findComponent(MainPlayer);
                if (player != null) player.parentComponent.removeComponent(player);
                
                for( c in Main.mainContainer.childComponents) {
                    if ((c is MainEditor)) continue;
                    c.parentComponent.removeComponent(c);
                }

                var editor = Main.mainContainer.findComponent(MainEditor);
                if (editor != null) {
                    editor.show();
                    return;
                }
                
                Main.mainContainer.addComponent(new MainEditor());
                buttonbar.hide();
        }
    }

    #end

    @:bind(open_player, MouseEvent.CLICK)
    private function onSelectButton(e:MouseEvent) {

                Main.mainContainer.removeAllComponents();
                Main.mainContainer.addComponent(new MainPlayer());
                
        #if haxeui_hxwidgets 
        buttonbar.hide();
        #end
    }

    @:bind(open_editor, MouseEvent.CLICK)
    private function onSelectButton2(e:MouseEvent) {
        Main.mainContainer.removeAllComponents();
                
                Main.mainContainer.addComponent(new MainEditor());
        #if haxeui_hxwidgets 
        buttonbar.hide();
        #end
    }
    



}