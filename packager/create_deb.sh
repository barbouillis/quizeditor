fpm -n quizeditor \
-s dir -t deb \
-p ../build/packages/quizeditor.deb \
--name quizeditor \
--license custom \
--version 0.0.1 \
--architecture all \
--description "Pour créer des quiz en format gift" \
--maintainer "Association Consonne" \
quizeditor.desktop=/usr/share/applications/quizeditor.desktop \
quiz.svg=/usr/share/icons/hicolor/scalable/apps/quizeditor.svg \
../build/hxwidgets/Main=/usr/local/bin/quizeditor


