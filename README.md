## What is Quiz Editor ?

It's a player and editor of the quiz format .gift .

Here is the description of the gift format [gift format](https://docs.moodle.org/401/en/GIFT_format)

![](doc/editor_screenshot.png)

## Prerequisites

- haxe
- haxeui-core  and depending on backends other haxeui   latest git
  ( you can follow the guides [here](http://haxeui.org/api/getting-started/backends/native-backends/haxeui-hxwidgets.html)

## Cloning the repository

git clone https://gitlab.com/barbouillis/quizeditor.git

git submodule update --init --recursive

## Building

Depending on the backend, if you want to build hxwidgets verions

```sh
    haxe hxwidgets.hxml
```
