package editor;

import haxe.ui.components.Label;
import haxe.ui.containers.HBox;
import quizparser.Answer;
import quizparser.Answer.AnswerNumerical;
import haxe.ui.containers.Card;
import haxe.ui.events.UIEvent;
import haxe.ui.events.FocusEvent;
import haxe.ui.events.MouseEvent;
import haxe.ui.components.NumberStepper;
import haxe.ui.components.Button;

@:build(haxe.ui.ComponentBuilder.build("assets/numerical-editor.xml"))
class NumericalEditor extends Card implements ICardEditor {

	public var userSetFractions:Map<Answer, Int> = new Map();

	public function new() {
		super();
	}

	@:bind(question_title, UIEvent.CHANGE)
	private function setTitle(e) {
		MainEditor.shownQuestion.title = question_title.text;
		var lv = rootComponent.findComponent("lv", haxe.ui.containers.ListView);
		lv.dataSource.update(lv.selectedIndex, {text: question_title.text, question: MainEditor.shownQuestion, origin:lv.selectedItem.origin});
	}

	@:bind(question_text, FocusEvent.FOCUS_OUT)
	private function setText(e) {
		MainEditor.shownQuestion.text = question_text.text;
	}

	@:bind(add_possibleAnswer, MouseEvent.CLICK)
	private function onMyButton2(e:MouseEvent) {
		addNumerical();
	}

	public function setToQuestion(q:quizparser.Question) {
        rootComponent.findComponent("feedback_editor", FeedbackEditor).setGeneralFeedback(q);
		question_title.text = q.title;
		question_text.text = q.text;
		for (a in q.answers.list) {
			var aa:AnswerNumerical = cast a;
			addNumerical(aa);
		}
        
	}

	public function addNumerical(?a:AnswerNumerical) {
		var an = a != null ? a : new AnswerNumerical();

		var hbox = new HBox();
		var fromLabel = new Label();
		fromLabel.text = "from";
		hbox.addComponent(fromLabel);
		var fromField = new NumberStepper();
		fromField.min = -999999;
        fromField.max = 999999;
		fromField.pos = 0;
		hbox.addComponent(fromField);
		var toLabel = new Label();
		toLabel.text = "to";
		hbox.addComponent(toLabel);
		var toField = new NumberStepper();
		toField.min = -999999;
        toField.max = 999999;
		toField.pos = 100;
		hbox.addComponent(toField);

		var button = new Button();
		button.text = "x";
		button.addClass("delete-button");
		hbox.addComponent(button);


		


		if (a != null) {
			///option.selected = (a.fraction == 100);
			//textfield.text = a.text;
		} else {
			an.answerType = quizparser.Answer.AnswerType.Numerical;
			an.fraction = 0;
			MainEditor.shownQuestion.answers.list.push(an);
		}

		an.text = an.minimumValue + ".." + an.maximumValue;

		button.onClick = function(e) {
			hbox.parentComponent.removeComponent(hbox);
			MainEditor.shownQuestion.answers.list.remove(an);
			rootComponent.findComponent("feedback_editor", FeedbackEditor).removeFeedback(an);
		}

		
		userSetFractions[an] = an.fraction;
		var isUserChanging = true;

		function onUserSetFraction(a:Answer) {
			
				if (isUserChanging )  {
					
					userSetFractions[an] = a.fraction;
				}
				else {
					trace("user changing");
				}
				isUserChanging = true;
		}

		

		rootComponent.findComponent("feedback_editor", FeedbackEditor).addFeedback(an, onUserSetFraction);
		fromField.onChange = function(e) {
			isUserChanging =false;
			an.minimumValue = fromField.value;
			an.text = an.minimumValue + ".." + an.maximumValue;
			rootComponent.findComponent("feedback_editor", FeedbackEditor).updateFeedback(an, isTiniestInterval(an));
		}
		
		toField.onChange = function(e) {
			isUserChanging = false;
			an.maximumValue = toField.value;
			an.text = an.minimumValue + ".." + an.maximumValue;
			//if (isTiniestInterval(an)) an.fraction = 100; else an.fraction = userSetFractions[an];
			rootComponent.findComponent("feedback_editor", FeedbackEditor).updateFeedback(an, isTiniestInterval(an));
		}

		/* not work here
		trace(an.minimumValue);
		fromField.pos = an.minimumValue;
		trace(fromField.value);
		toField.pos = an.maximumValue;
		*/

		options.addComponent(hbox);
		fromField.pos = an.minimumValue;
		toField.pos = an.maximumValue;
		
	}

	private function isTiniestInterval(an:AnswerNumerical) {
		var tiniest = 99999999.;
		var q  = MainEditor.shownQuestion;
		
		var tinyAnswer = an;
		for (a in q.answers.list) {
			var aa:AnswerNumerical = cast a;
			a.fraction = userSetFractions[a];
		}

		for (a in q.answers.list) {
			var aa:AnswerNumerical = cast a;
			var interval = Math.abs(aa.maximumValue - aa.minimumValue);
			
			
			if (interval < tiniest) {
				tinyAnswer.fraction = userSetFractions[tinyAnswer];
				
				//trace(userSetFractions[tinyAnswer]);
				
				tiniest = interval;
				tinyAnswer = aa;
				tinyAnswer.fraction = 100;
			}
			
		}

		trace(userSetFractions);

		for (a in q.answers.list) {
			var aa:AnswerNumerical = cast a;
			rootComponent.findComponent("feedback_editor", FeedbackEditor).updateFeedback(aa, false);
		}
		//trace(userSetFractions);
		trace(an==tinyAnswer);
		return (an==tinyAnswer);
	}
}
