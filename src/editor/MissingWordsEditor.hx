package editor;

import haxe.ui.containers.Card;
import haxe.ui.events.UIEvent;
import haxe.ui.events.MouseEvent;
import haxe.ui.events.FocusEvent;
import haxe.ui.components.CheckBox;
import haxe.ui.components.TextField;
import haxe.ui.containers.HBox;
import haxe.ui.components.Button;
import quizparser.Answer;

@:build(haxe.ui.ComponentBuilder.build("assets/missingwords-editor.xml"))
class MissingWordsEditor extends Card implements ICardEditor {
	public function new() {
		super();
	}

	@:bind(question_title, UIEvent.CHANGE)
	private function setTitle(e) {
		MainEditor.shownQuestion.title = question_title.text;
		var lv = rootComponent.findComponent("lv", haxe.ui.containers.ListView);
		lv.dataSource.update(lv.selectedIndex, {text: question_title.text, question: MainEditor.shownQuestion, origin:lv.selectedItem.origin});
	}

	@:bind(question_text, FocusEvent.FOCUS_OUT)
	private function setText(e) {
		var i = question_text.text.indexOf("___");
		if (i>1) {
			MainEditor.shownQuestion.text = question_text.text.substr(0,i);
			MainEditor.shownQuestion.textAfter = question_text.text.substr(i+3);
		}
		else {
			MainEditor.shownQuestion.text = question_text.text;
			MainEditor.shownQuestion.textAfter = "";
		}
		
	}

	@:bind(add_option, MouseEvent.CLICK)
    private function onMyButton2(e:MouseEvent) {
        addOptionBox();
       
    }

	public function addOptionBox(?a:Answer) {
		var an = a != null ? a : new quizparser.Answer();
		var hbox = new HBox();

		var option = new CheckBox();
		hbox.addComponent(option);
		var textfield = new TextField();
		textfield.placeholder = "Possible Answer";
		hbox.addComponent(textfield);
				var button = new Button();
		button.text = "x";
		button.addClass("delete-button");
		hbox.addComponent(button);

		button.onClick = function(e) {
			options.removeComponent(hbox);
			MainEditor.shownQuestion.answers.list.remove(an);
			rootComponent.findComponent("feedback_editor", FeedbackEditor).removeFeedback(an);
		}

		options.addComponent(hbox);

		if (a != null) {
			option.selected = (a.fraction > 0);
			textfield.text = a.text;
		} else {
			an.answerType = quizparser.Answer.AnswerType.Multiple;
			an.fraction = 0;
			MainEditor.shownQuestion.answers.list.push(an);
		}

		option.userData = an;

		rootComponent.findComponent("feedback_editor", FeedbackEditor).addFeedback(an);
		textfield.onChange = function(e) {
			
			an.text = textfield.text;
            rootComponent.findComponent("feedback_editor", FeedbackEditor).updateFeedback(an);
		}
		option.onChange = function(e) {
			if (option.selected){
				if (an.fraction <= 0) an.fraction = 100;
			}
			else {
				if (an.fraction > 0) an.fraction = 0;
			}
			rootComponent.findComponent("feedback_editor", FeedbackEditor).updateFeedback(an);
		}
	}

	public function setToQuestion(q:quizparser.Question) {
		rootComponent.findComponent("feedback_editor", FeedbackEditor).setGeneralFeedback(q);
		question_title.text = q.title;
		question_text.text = q.text + "___" + q.textAfter;

		for (a in q.answers.list) {
			addOptionBox(a);
		}
		
	}
}
