package editor;

import haxe.ui.containers.Card;
import haxe.ui.events.UIEvent;
import haxe.ui.events.FocusEvent;

@:build(haxe.ui.ComponentBuilder.build("assets/description-editor.xml"))
class DescriptionEditor extends Card implements ICardEditor {

    public function new() {
        super();
    }

    @:bind(question_title, UIEvent.CHANGE)   
    private function setTitle(e) {
        MainEditor.shownQuestion.title = question_title.text;
        var lv = rootComponent.findComponent("lv", haxe.ui.containers.ListView);
        lv.dataSource.update(lv.selectedIndex, {text:question_title.text, question:MainEditor.shownQuestion});
    }

    @:bind(question_text, FocusEvent.FOCUS_OUT)   
    private function setText(e) {
        MainEditor.shownQuestion.text = question_text.text;
    }

    public function setToQuestion(q:quizparser.Question) {
        question_title.text = q.title;
        question_text.text  = q.text;
    }

}