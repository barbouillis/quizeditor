package editor;

import haxe.ui.containers.Card;
import haxe.ui.events.UIEvent;
import haxe.ui.events.FocusEvent;

@:build(haxe.ui.ComponentBuilder.build("assets/truefalse-editor.xml"))
class TrueFalseEditor extends Card implements ICardEditor {
	public function new() {
		super();
		#if !haxeui_hxwidgets setAnswer(null); #end
	}

	@:bind(question_title, UIEvent.CHANGE)
	private function setTitle(e) {
		MainEditor.shownQuestion.title = question_title.text;
		var lv = rootComponent.findComponent("lv", haxe.ui.containers.ListView);
		lv.dataSource.update(lv.selectedIndex, {text: question_title.text, question: MainEditor.shownQuestion, origin: lv.selectedItem.origin});
	}

	@:bind(question_text, FocusEvent.FOCUS_OUT)
	private function setText(e) {
		MainEditor.shownQuestion.text = question_text.text;
	}

	@:bind(button_true, UIEvent.CHANGE)
	private function setButton(e) {
		cast(MainEditor.shownQuestion.answers.list[0], quizparser.Answer.AnswerTrueFalse).result = button_true.selected;
		cast(MainEditor.shownQuestion.answers.list[0], quizparser.Answer.AnswerTrueFalse).text = "" + button_true.selected;
		rootComponent.findComponent("feedback_editor", FeedbackEditor)
			.updateFeedback(cast(MainEditor.shownQuestion.answers.list[0], quizparser.Answer.AnswerTrueFalse));
	}

    @:bind(button_false, UIEvent.CHANGE)
	private function setButtonF(e) {
		cast(MainEditor.shownQuestion.answers.list[1], quizparser.Answer.AnswerTrueFalse).result = button_false.selected;
		cast(MainEditor.shownQuestion.answers.list[1], quizparser.Answer.AnswerTrueFalse).text = "" + button_false.selected;
		rootComponent.findComponent("feedback_editor", FeedbackEditor)
			.updateFeedback(cast(MainEditor.shownQuestion.answers.list[1], quizparser.Answer.AnswerTrueFalse));
	}

	@:bind(button_true, UIEvent.READY)
	private function setAnswer(e) {
		if (MainEditor.shownQuestion.answers.list.length == 0) {
			var an = new quizparser.Answer.AnswerTrueFalse();
			an.answerType = quizparser.Answer.AnswerType.TrueFalse;
			an.result = false;
			an.text = "" + false;

			MainEditor.shownQuestion.answers.list.push(an);
			MainEditor.shownQuestion.answers.completeTrueFalse();
		}
	}

	public function setToQuestion(q:quizparser.Question) {
		question_title.text = q.title;
		question_text.text = q.text;
		if (q.answers.list.length == 0) {
			var an = new quizparser.Answer.AnswerTrueFalse();
			an.result = true;
			q.answers.list.push (an);
		}
		var result = cast(q.answers.list[0], quizparser.Answer.AnswerTrueFalse).result;
		if (result) {
			button_true.selected = true;
		} else {
			button_false.selected = false;
		}

		MainEditor.shownQuestion.answers.completeTrueFalse();

		rootComponent.findComponent("feedback_editor", FeedbackEditor).setGeneralFeedback(q);
		rootComponent.findComponent("feedback_editor", FeedbackEditor).addFeedback(cast(q.answers.list[0], quizparser.Answer.AnswerTrueFalse), false);
		rootComponent.findComponent("feedback_editor", FeedbackEditor).addFeedback(cast(q.answers.list[1], quizparser.Answer.AnswerTrueFalse), false);
	}
}
