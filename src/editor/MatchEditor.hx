package editor;

import quizparser.Answer.AnswerMatch;
import haxe.ui.containers.Card;
import haxe.ui.events.MouseEvent;
import haxe.ui.events.UIEvent;
import haxe.ui.events.FocusEvent;
import haxe.ui.components.OptionBox;
import haxe.ui.components.TextField;
import haxe.ui.containers.HBox;
import haxe.ui.components.Button;

@:build(haxe.ui.ComponentBuilder.build("assets/matching-editor.xml"))
class MatchEditor extends Card implements ICardEditor {
	public function new() {
		super();
	}

	@:bind(question_title, UIEvent.CHANGE)
	private function setTitle(e) {
		MainEditor.shownQuestion.title = question_title.text;
		var lv = rootComponent.findComponent("lv", haxe.ui.containers.ListView);
		lv.dataSource.update(lv.selectedIndex, {text: question_title.text, question: MainEditor.shownQuestion, origin:lv.selectedItem.origin});
	}

	@:bind(question_text, FocusEvent.FOCUS_OUT)
	private function setText(e) {
		MainEditor.shownQuestion.text = question_text.text;
	}

	@:bind(add_pair, MouseEvent.CLICK)
	private function onMyButton2(e:MouseEvent) {
		addOptionBox();
	}

	public function addOptionBox(?a:AnswerMatch) {
		var an = new quizparser.Answer.AnswerMatch();
		an.answerType = quizparser.Answer.AnswerType.Match;

		if (a != null)
			an = a;

		var textfieldL = new TextField();
		textfieldL.placeholder = "Match";
		pairs.addComponent(textfieldL);

		textfieldL.onChange = function(e) {
			an.textLeft = textfieldL.text;
			rootComponent.findComponent("feedback_editor", FeedbackEditor).updateFeedback(an);
		}

		var textfieldR = new TextField();
		textfieldR.placeholder = "Match";
		pairs.addComponent(textfieldR);

		textfieldR.onChange = function(e) {
			an.textRight = textfieldR.text;
			an.text = an.textLeft + " -> " + an.textRight;

			rootComponent.findComponent("feedback_editor", FeedbackEditor).updateFeedback(an);
		}

		var button = new Button();
		button.text = "x";
		button.addClass("delete-button");
		pairs.addComponent(button);

		button.onClick = function(e) {
			pairs.removeComponent(button);
			pairs.removeComponent(textfieldL);
			pairs.removeComponent(textfieldR);
			MainEditor.shownQuestion.answers.list.remove(an);
			rootComponent.findComponent("feedback_editor", FeedbackEditor).removeFeedback(an);
		}

		rootComponent.findComponent("feedback_editor", FeedbackEditor).addFeedback(an, false);

		if (a != null) {
			var m:AnswerMatch = cast a;
			textfieldL.text = m.textLeft;
			textfieldR.text = m.textRight;
			return;
		}

		MainEditor.shownQuestion.answers.list.push(an);
	}

	public function setToQuestion(q:quizparser.Question) {
		rootComponent.findComponent("feedback_editor", FeedbackEditor).setGeneralFeedback(q);
		question_title.text = q.title;
		question_text.text = q.text;

		for (a in q.answers.list) {
			addOptionBox(cast a);
		}
	}
}
