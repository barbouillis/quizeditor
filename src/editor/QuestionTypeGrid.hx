package editor;

import haxe.ui.containers.Grid;
import haxe.ui.events.MouseEvent;
import haxe.ui.events.UIEvent;
import quizparser.Question.QuestionType;
import haxe.ui.core.Component;

@:build(haxe.ui.ComponentBuilder.build("assets/questiontype-grid.xml"))
class QuestionTypeGrid extends Grid
{

    public var map:Map<Component, {type:QuestionType, editor:Class<ICardEditor>, text:String, feedback:Bool}>;  
    public function new() {
        super();
        columns = 2 ;

        map = [
            true_false     => {type:TrueFalse, editor:TrueFalseEditor, text:"{{TYPE_TRUE_FALSE}}", feedback:true},
            short          => {type:Short, editor:ShortEditor, text:"{{TYPE_SHORT}}", feedback:true},
            description    => {type:Description, editor:DescriptionEditor, text:"{{TYPE_DESCRIPTION}}", feedback:false},
            numerical      => {type:Numerical, editor:NumericalEditor, text:"{{TYPE_NUMERICAL}}", feedback:true},
            match          => {type:Match, editor:MatchEditor, text:"{{TYPE_MATCH}}", feedback:true},
            essay          => {type:Essay, editor:EssayEditor, text:"{{TYPE_ESSAY}}", feedback:false},
            missingWord    => {type:MissingWords, editor:MissingWordsEditor, text:"{{TYPE_MISSINGWORD}}", feedback:true},
            multiAnswers   => {type:MultiAnswers, editor:MultiAnswersEditor, text:"{{TYPE_MULTIANSWERS}}", feedback:true},
            multiplechoice => {type:Multichoice, editor:MultipleChoiceEditor, text:"{{TYPE_MULTICHOICE}}", feedback:true},
        ];

    }

    /*
	Description;
	Essay;
	Numerical;
	MissingWords;
	Multichoice;
	MultiAnswers;
	Match;
	Short;
	TrueFalse;
    */


    @:bind(true_false, MouseEvent.CLICK)
    @:bind(short, MouseEvent.CLICK)
    @:bind(description, MouseEvent.CLICK)
    @:bind(numerical, MouseEvent.CLICK)
    @:bind(match, MouseEvent.CLICK)
    @:bind(essay, MouseEvent.CLICK)
    @:bind(missingWord, MouseEvent.CLICK)
    @:bind(multiAnswers, MouseEvent.CLICK)
    @:bind(multiplechoice, MouseEvent.CLICK)
    private function clickOnQuestion(e:MouseEvent) {
        setNewQuestion(map[e.target].type);
        MainEditor.shownQuestion.title = map[e.target].text;
        var card = parentComponent;
        if (map[e.target].feedback) showFeedback();
        card.removeAllComponents();
        var cardEditor =  Type.createInstance(map[e.target].editor, []);
        card.findAncestor(MainEditor).findComponent("feedback_editor", FeedbackEditor).reset();
        trace(card);
        card.addComponent(cast(cardEditor,Component));
        cardEditor.setToQuestion(MainEditor.shownQuestion);
        if ((cardEditor is MultipleChoiceEditor)) {
            cast(cardEditor, MultipleChoiceEditor).addOptionBox();
            cast(cardEditor, MultipleChoiceEditor).addOptionBox();
        }
        var item = {text:map[e.target].text, question:MainEditor.shownQuestion};
        var lv = card.rootComponent.findComponent("lv", haxe.ui.containers.ListView);
        lv.dataSource.update(lv.selectedIndex, item);
        
    }

    private function setNewQuestion(type:QuestionType) {
        MainEditor.shownQuestion.type  = type;

        MainEditor.shownQuestion.answers = new quizparser.Answers();
        MainEditor.shownQuestion.answers.type = MainEditor.shownQuestion.type;

        MainEditor.quizBank.addQuestion(MainEditor.shownQuestion);

        rootComponent.findComponent("tagsbox", Component).show();

        //rootComponent.findComponent("feedback_editor", FeedbackEditor).show();
        //rootComponent.findComponent("feedback_editor", FeedbackEditor).hidden = false;
    }
    private function showFeedback() {
        rootComponent.findComponent("feedback_editor", FeedbackEditor).show();
        rootComponent.findComponent("feedback_editor", FeedbackEditor).hidden = false;
    }
}