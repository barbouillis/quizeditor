package editor;

import haxe.ui.HaxeUIApp;
import haxe.ui.Toolkit;
import quizparser.Question;
import haxe.ui.components.NumberStepper;
import haxe.ui.containers.Grid;
import haxe.ui.components.Label;
import haxe.ui.components.TextArea;
import haxe.ui.containers.Card;
import haxe.ui.events.FocusEvent;
import haxe.ui.events.UIEvent;
import haxe.ui.containers.VBox;
import haxe.ui.containers.dialogs.Dialog.DialogButton;

@:xml('
<card>
<grid id="grid" columns="2" width="100%"/>
</card>
')
class FeedbackEditor extends Card {
	public var feedbacks:Map<String, String> = new Map();

	public var hideFractions = false;

	public function new() {
		super();
		text = "{{FEEDBACKS}}";
	}

	public function setGeneralFeedback(q:Question) {
		var label = new Label();
		var textA = new TextArea();

		textA.addClass("big_feedback_text");
		textA.registerEvent(UIEvent.SHOWN, function(e) {
			textA.text = q.generalFeedback;
		});

		textA.registerEvent(FocusEvent.FOCUS_OUT, function(e) {
			q.generalFeedback = textA.text;
		});
		label.htmlText = "<b>{{GENERAL_FEEDBACK}}</b>";
		grid.addComponent(label);
		var textLabel = new Label();
		var message = "{{MISSING_FEEDBACK}}";
		textLabel.text = q.generalFeedback == "" ? message : q.generalFeedback;
		textLabel.addClass("little_feedback_text");
		textLabel.registerEvent(UIEvent.SHOWN, function(e) {
			textLabel.text = q.generalFeedback == "" ? message : q.generalFeedback;
		});
		textLabel.onClick = function(e) {
			var dia = new feedback.FeedbackEditorDialog(q.title);
			if (q.generalFeedback != "")
				dia.textarea.text = textLabel.text;
			dia.onDialogClosed = function(e) {
				if (e.button == DialogButton.APPLY) {
					textLabel.text = dia.textarea.text;
					if (textLabel.text != message)
						q.generalFeedback = textLabel.text;
				}
			}
			dia.show();
		}
		grid.addComponent(textLabel);
		grid.addComponent(textA);
	}

	public function addFeedback(answer:quizparser.Answer, hasFraction:Bool = true, fractionCallback:quizparser.Answer->Void = null) {
		var label = new Label();
		label.text = answer.text;
		label.addClass("label_answer");
		label.userData = answer;
		label.customStyle.minWidth = 100;
		if (!hasFraction)
			label.height = 40;

		var textArea = new TextArea();
		textArea.text = answer.feedback;
		textArea.addClass("textarea_answer");
		textArea.addClass("big_feedback_text");
		textArea.userData = answer;
		textArea.customStyle.percentWidth = 100;
		textArea.customStyle.percentHeight = 100;
		textArea.horizontalAlign = "center";
		textArea.registerEvent(FocusEvent.FOCUS_OUT, function(e) {
			answer.feedback = textArea.text;
		});
		textArea.registerEvent(UIEvent.SHOWN, function(e) {
			textArea.text = answer.feedback;
		});

		var textLabel = new Label(); // TextArea();
		// textLabel.hidden = bigScreen;
		// trace(textLabel.hidden);
		var message = "missing feedback - click to fill";
		textLabel.text = answer.feedback == "" ? message : answer.feedback;
		textLabel.addClass("textarea_answer");
		textLabel.addClass("little_feedback_text");
		textLabel.userData = answer;
		textLabel.customStyle.percentWidth = 100;
		textLabel.customStyle.percentHeight = 100;
		textLabel.horizontalAlign = "center";
		textLabel.verticalAlign = "center";
		textLabel.onClick = function(e) {
			var dia = new feedback.FeedbackEditorDialog(answer.text);
			if (answer.feedback != "")
				dia.textarea.text = textLabel.text;
			dia.onDialogClosed = function(e) {
				if (e.button == DialogButton.APPLY) {
					textLabel.text = dia.textarea.text;
					if (textLabel.text != message)
						answer.feedback = textLabel.text;
				}
			}
			dia.show();
		}
		textLabel.registerEvent(UIEvent.SHOWN, function(e) {
			textLabel.text = textLabel.text != message ? answer.feedback : message;
		});

		var vbox = new VBox();
		vbox.addComponent(label);
		if (hasFraction) {
			var nbrStepper = new NumberStepper();
			nbrStepper.addClass("stepper_answer");
			nbrStepper.userData = answer;
			nbrStepper.hidden = hideFractions;
			nbrStepper.min = -999999;
			nbrStepper.max = 999999;
			nbrStepper.value = answer.fraction;
			nbrStepper.onChange = function(e) {
				answer.fraction = nbrStepper.value;
				if (fractionCallback != null)
					fractionCallback(answer);
			};
			// nbrStepper.customStyle.paddingLeft = 40;
			vbox.addComponent(nbrStepper);
		}
		vbox.horizontalAlign = "center";

		// grid.addComponent(label);
		// grid.addComponent(nbrStepper);
		grid.addComponent(vbox);
		grid.addComponent(textArea);
		grid.addComponent(textLabel);
	}

	public function removeFeedback(answer:quizparser.Answer) {
		for (l in grid.findComponents("label_answer", Label)) {
			if (l.userData == answer) {
				grid.removeComponent(l);
			}
		}

		for (l in grid.findComponents("textarea_answer", TextArea)) {
			if (l.userData == answer) {
				grid.removeComponent(l);
			}
		}

		for (l in grid.findComponents("stepper_answer", NumberStepper)) {
			if (l.userData == answer) {
				grid.removeComponent(l);
			}
		}
	}

	/**
		Updates the answer - block fraction will disable the user input in fraction
		You can only block one at the time ??? To check
	**/
	public function updateFeedback(answer:quizparser.Answer, blockFraction:Bool = false) {
		for (l in findComponents("label_answer", Label)) {
			if (l.userData == answer) {
				l.text = answer.text;
				break;
			}
		}

		for (l in findComponents("textarea_answer", TextArea)) {
			if (l.userData == answer) {
				l.text = answer.feedback;
				break;
			}
		}

		for (l in findComponents("stepper_answer", NumberStepper)) {
			l.disabled = false;
			if (l.userData == answer) {
				l.value = answer.fraction;
				l.disabled = blockFraction;
			}
		}
	}

	public function reset() {
		grid.removeAllComponents();
	}
}
