package editor;

import haxe.ui.components.Button;
import haxe.ui.containers.Card;
import haxe.ui.events.MouseEvent;
import haxe.ui.events.UIEvent;
import haxe.ui.events.FocusEvent;
import haxe.ui.components.OptionBox;
import haxe.ui.components.TextField;
import haxe.ui.containers.HBox;
import quizparser.Answer;

@:build(haxe.ui.ComponentBuilder.build("assets/multiplechoice.xml"))
class MultipleChoiceEditor extends Card implements ICardEditor {
	public function new() {
		super();
	}

	@:bind(question_title, UIEvent.CHANGE)
	private function setTitle(e) {
		MainEditor.shownQuestion.title = question_title.text;
		var lv = rootComponent.findComponent("lv", haxe.ui.containers.ListView);
		lv.dataSource.update(lv.selectedIndex, {text: question_title.text, question: MainEditor.shownQuestion, origin:lv.selectedItem.origin});
	}

	@:bind(question_text, FocusEvent.FOCUS_OUT)
	private function setText(e) {
		MainEditor.shownQuestion.text = question_text.text;
	}

	@:bind(add_option, MouseEvent.CLICK)
	private function onMyButton2(e:MouseEvent) {
		addOptionBox();
	}

	public function addOptionBox(?a:Answer) {
		var an = a != null ? a : new quizparser.Answer();

		var hbox = new HBox();
		var oButton  = new Button();
		oButton.componentGroup = "myGroup";
		oButton.addClass("myGroup");
		oButton.toggle = true;
		hbox.addComponent(oButton);
		var textfield = new TextField();
		textfield.placeholder = "Possible Answer";
		hbox.addComponent(textfield);
		var button = new Button();
		button.text = "x";
		button.addClass("delete-button");
		hbox.addComponent(button);

		button.onClick = function(e) {
			options.removeComponent(hbox);
			MainEditor.shownQuestion.answers.list.remove(an);
			rootComponent.findComponent("feedback_editor", FeedbackEditor).removeFeedback(an);
		}

		options.addComponent(hbox);

		if (a != null) {
			oButton.selected = (a.fraction == 100);
			textfield.text = a.text;
		} else {
			an.answerType = quizparser.Answer.AnswerType.Multiple;
			an.fraction = 0;
			oButton.selected = false;
			MainEditor.shownQuestion.answers.list.push(an);
		}

		oButton.userData = an;

		rootComponent.findComponent("feedback_editor", FeedbackEditor).addFeedback(an, false);
		textfield.onChange = function(e) {
			
			an.text = textfield.text;
			rootComponent.findComponent("feedback_editor", FeedbackEditor).updateFeedback(an);
		}
		oButton.onChange = function(e) {
			#if haxeui_hxwidgets
			for (o in findComponents("myGroup", Button) ) { //ButtonGroups.instance.get("myGroup")
				if (o.userData != null) {
					cast(o.userData, quizparser.Answer).fraction = o.selected ? 100: 0;
					rootComponent.findComponent("feedback_editor", FeedbackEditor).updateFeedback(cast(o.userData, quizparser.Answer));

				}
			}
			#end
			/*

			if (oButton.selected) {
				an.fraction = 100;
				rootComponent.findComponent("feedback_editor", FeedbackEditor).updateFeedback(cast(oButton.userData, quizparser.Answer), true);
			}
			else {
				#if !haxeui_hxwidgets
				for (o in ButtonGroups.instance.get("myGroup")) {
				if (oButton.userData != null) {
					cast(oButton.userData, quizparser.Answer).fraction = 0;
					rootComponent.findComponent("feedback_editor", FeedbackEditor).updateFeedback(cast(oButton.userData, quizparser.Answer));
				}
			}
				#end

				rootComponent.findComponent("feedback_editor", FeedbackEditor).updateFeedback(an);
			}*/

		}
	}

	public function setToQuestion(q:quizparser.Question) {
        rootComponent.findComponent("feedback_editor", FeedbackEditor).setGeneralFeedback(q);
		question_title.text = q.title;
		question_text.text = q.text;
		for (a in q.answers.list) {
			addOptionBox(a);
		}
        
	}
}
