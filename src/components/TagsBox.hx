package components;


import haxe.ui.components.Label;
import haxe.ui.containers.VBox;
import haxe.ui.containers.HBox;
import haxe.ui.events.UIEvent;
import haxe.ui.components.Button;
import haxe.ui.core.Component;
import haxe.ui.containers.dialogs.Dialog.DialogButton;

#if haxeui_hxwidgets import hx.widgets.TextCtrl; #end

@:xml('
<vbox width="100%">
    <hbox width="100%">
	<label text="{{SELECT_TAG}}" />
    <dropdown id="tags_dropdown" />
    <label text="{{OR_CREATE_TAG}}" />
    <textfield id="tag_textfield"/>
    </hbox>
    <hbox id="tags_box" continuous="true"/>
</vbox>
')
class TagsBox extends VBox {

	@:bind(this, UIEvent.READY)
	private function initDropdown(e) {
		resetTagsDropdown();
	}

	public function reset() {
		tags_box.removeAllComponents();
		if (MainEditor.shownQuestion == null) return;
		for (  t in MainEditor.shownQuestion.tags) {
			trace(t);
			createTagButton(t);
		}
		resetTagsDropdown();
		#if haxeui_hxwidgets
		var textCtrl = cast (tag_textfield.window, TextCtrl);
		textCtrl.autoComplete(MainEditor.quizBank.allTags());
		#end
	

	}

	public function resetTagsDropdown() {
		tags_dropdown.dataSource.clear();
		tags_dropdown.dataSource.allowCallbacks = false;
		for (t in MainEditor.quizBank.allTags()) {
			if (MainEditor.shownQuestion.tags.contains(t)) continue;
			tags_dropdown.dataSource.add(t);
		}
		tags_dropdown.dataSource.allowCallbacks = true;
		tags_dropdown.disabled = (tags_dropdown.dataSource.size  == 0 );
	}

	@:bind(tags_dropdown, UIEvent.CHANGE)
	private function onTagsDropdown(e) {
		addTagButton(tags_dropdown.selectedItem);
	}
    
    @:bind(tag_textfield, UIEvent.SUBMIT)
    private function validateTextField (e) {
        addTagButton(tag_textfield.text);
        tag_textfield.text = "";
    }

	public function createTagButton(text:String) {

		#if haxeui_hxwidgets
		var button = new Button();
		button.text = "" + text;
		button.styleString = "icon:$close";
		

		button.onClick = function(e) {
			var dia = haxe.ui.containers.dialogs.Dialogs.messageBox("{{CONFIRMATION_TAG}}"+ text, 'Question', 'question', function f(but) {
				if (but == DialogButton.YES) {
					MainEditor.shownQuestion.tags.remove(button.text);
					tags_dropdown.dataSource.add(button.text);
					tags_box.removeComponent(button);
				}
			});
		}

		tags_box.addComponent(button);
		#else


		#end



	}

    private function addTagButton(text:String) {
		if (MainEditor.shownQuestion == null) return;
        if (!MainEditor.shownQuestion.tags.contains(text)) {
			MainEditor.shownQuestion.tags.push(text);
			createTagButton(text);
        }
        else {
            return;
        }
        
		#if true
		
		#else

		var button = new Button();
		button.text = "" + text;

		button.styleString = "icon:$close";
		button.iconPosition = "right";

		tags_box.addComponent(button);

		tags_dropdown.dataSource.remove(text);

		
		button.registerEvent(UIEvent.READY, function f(e) {
			button.findComponent("button-icon", Component, false).onClick = function(e) {
				MainEditor.shownQuestion.tags.remove(button.text);
				tags_box.removeComponent(button);
				resetTagsDropdown();

				/*
				if (tagsDropdown.dataSource.indexOf(button.userData) < 0)
					tagsDropdown.dataSource.add(button.userData);*/
			}
		});

		#end

		/*

		var tagToRemove = data;
		if (tagToRemove == null) {
			for (i in 0...tagsDropdown.dataSource.size) {
				if (tagsDropdown.dataSource.get(i).text == button.text) {
					tagToRemove = tagsDropdown.dataSource.get(i);
					break;
				}
			}
		}
		button.userData = tagToRemove;

		haxe.ui.util.Timer.delay(function f() {
			tagsDropdown.selectedIndex = -1;
			tagsDropdown.selectedItem = null;
			tagsDropdown.text = " Ajouter Tag";
			tagsDropdown.invalidateComponentDisplay();
			tagsDropdown.invalidateComponent();
			tagsDropdown.dataSource.remove(button.userData);
			tagsDropdown.dataSource.clearFilter();
			tagsDropdown.searchField.text = "";
		}, 600);*/
	}
    

}