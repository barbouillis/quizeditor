import haxe.ui.components.Label;
import haxe.ui.core.Component;
import quizparser.QuizBank;
import quizparser.QuizBankPlayer;
import haxe.ui.containers.ScrollView;
import haxe.ui.containers.VBox;
import haxe.ui.containers.ScrollView;

class CorrectQuizBank extends ScrollView {

    public function new() {
        super();
        var label = new Label();
        label.text = "Correction for Quiz";
        addComponent(label);
        percentContentWidth =100;
    }

    public function showQuestionBank(qb:QuizBank, qbp: QuizBankPlayer) {

        if (findComponent("vbox_cards", Component)!= null) {
            this.removeComponent(findComponent("vbox_cards", Component));
        }
        var vbox = new VBox();
        vbox.id = "vbox_cards";
        this.addComponent(vbox);

        trace("aaaaa");
        trace(qb);
        trace(qb.questions);

        for ( q in qb.questions) {
            trace(q);
            var card = UIUtils.createCard(q);
            card.questionAttempt = qbp.attemptForQuestion(q);
            card.disabled = true;
            card.customStyle.minWidth = 300;
            card.removeInteractivity();
            card.showCorrect();

            
            vbox.addComponent(card);

        }
        


    }

}