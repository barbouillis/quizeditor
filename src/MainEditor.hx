package;

#if sys import sys.io.File; #end
import haxe.ui.locale.LocaleManager;
import haxe.ui.components.Label;
import haxe.ui.core.Component;
import quizparser.QuizBank;
import quizparser.Question;
import haxe.ui.containers.VBox;
import haxe.ui.events.MouseEvent;
import haxe.ui.events.UIEvent;
import haxe.ui.components.Button;
#if js
import js.html.URL;
#end

@:build(haxe.ui.ComponentBuilder.build("assets/main-view.xml"))
class MainEditor extends VBox {
	public static var loadDefaultBank:Bool = true;
	public static var shownQuestion:Question = null;
	public static var quizBank:QuizBank = new QuizBank();

	#if (js || haxeui_hxwidgets)
	@:bind(this, UIEvent.SHOWN)
	private function setupDrop(e) {
		UIUtils.setupDrop(drop_box, loadGift);
	}
	#end

	#if sys
	@:bind(this, UIEvent.READY)
	private function initLoad(e) {
		if (!loadDefaultBank) return;
		if (Main.openedPath != null) {
			var file = File.getContent(Main.openedPath);
			loadGift(file, Main.openedPath);
			loadDefaultBank = false;
		}
	}
	#end

	#if haxeui_hxwidgets
	@:bind(language, UIEvent.READY)
	private function initLanguageDropdown(e) {
		for ( i in 0...language.dataSource.size) {
			if (language.dataSource.get(i).locale ==  LocaleManager.instance.language) {
				language.selectedIndex = i;
				break;
			}
		}
	}
	#end

	@:bind(language, UIEvent.CHANGE)
	private function changeLanguage(e) {
		haxe.ui.locale.LocaleManager.instance.language = language.text.toLowerCase();
	}

	@:bind(delete_card, MouseEvent.CLICK)
	private function deleteCard(e) {
		quizBank.removeQuestion(shownQuestion);
		shownQuestion = null;
		lv.dataSource.remove(lv.selectedItem);
		play_card.removeAllComponents();
		editor.removeAllComponents();

		tagsbox.reset();
		feedback_editor.reset();
		feedback_editor.hide();
		tagsbox.hide();

		lv.selectedIndex = Std.int(Math.max(0,lv.selectedIndex-1));	
	}

	/*

	#if !haxeui_hxwidgets
	@:bind(tagsDropdown.searchField, UIEvent.INITIALIZE)
	private function tagsDropdownWriteInit(e) {
		tagsDropdown.searchField.registerEvent(haxe.ui.events.KeyboardEvent.KEY_DOWN, function(e:haxe.ui.events.KeyboardEvent) {
			if (e.keyCode == 13) {
				if (tagsDropdown.dataSource.size == 0) {
					tagsDropdown.dataSource.add({text: "" + tagsDropdown.searchField.text});
					tagsDropdown.hideDropDown();
					e.cancel();
					tagsDropdown.selectedItem = {text: "" + tagsDropdown.searchField.text};
					tagsDropdown.dataSource.clearFilter();
				}
			}
		});
	}
	#end*/

	@:bind(add_question, MouseEvent.CLICK)
	private function onMyButton(e:MouseEvent) {
		properties.show();
		properties.hidden = false;
		properties.walkComponents(function f(c) {c.hidden = false; return true;});
		MainEditor.shownQuestion = new quizparser.Question();
		var item:{} = {text: "new Question", question: MainEditor.shownQuestion, origin: "Crée ici"};
		lv.dataSource.add(item);
		lv.selectedIndex = lv.dataSource.indexOf(item);
		drop_indication.hide();
	}

	#if !haxeui_raylib
	@:bind(load_quizbank, MouseEvent.CLICK)
	private function loadQuiz(e:MouseEvent) {
		haxe.ui.containers.dialogs.Dialogs.openFile(function(b, files) {
			if (b == haxe.ui.containers.dialogs.Dialog.DialogButton.OK) {
				loadGift(files[0].text, files[0].name);
			}
		}, {
			extensions: [{extension: "txt,gift", label: "Text Files"},],
			readContents: true
		});
	}

	@:bind(save_quizbank, MouseEvent.CLICK)
	private function saveQuiz(e:MouseEvent) {
		haxe.ui.containers.dialogs.Dialogs.saveTextFile("Save Text File", [{extension: "txt,gift", label: "Text Files"},],
			{name: "My Text File.gift", text: saveGift()}, function(b, s) {});
	}
	#end

	private function loadGift(gift:String, origin:String) {
		drop_indication.hide();
		quizBank = new QuizBank();
		quizBank.loadBankFromString(gift);
		lv.dataSource.allowCallbacks = false; // speeds things up a little
		for (q in quizBank.questions) { // Should do it by id order
			lv.dataSource.add({text: q.title, question: q, origin: origin});
		}
		lv.dataSource.allowCallbacks = true;
		showPreviewAll(null);
	}

	/*
	private function resetTagsDropdown() {
		tagsDropdown.dataSource.clear();
		tagsDropdown.dataSource.allowCallbacks = false;
		for (t in quizBank.allTags()) {
			tagsDropdown.dataSource.add({text: t});
		}
		tagsDropdown.dataSource.allowCallbacks = true;
	}*/

	private function saveGift() {
		quizBank = new QuizBank();

		for (i in 0...lv.dataSource.size) {
			var q = lv.dataSource.get(i).question;
			quizBank.addQuestion(q);
		}

		return quizBank.toGift();
	}

	@:bind(tab, UIEvent.CHANGE)
	private function showPreviewAll(e:UIEvent) {
		if (tab.selectedPage == preview_giftbank) {
			preview_all.showQuestionBank(quizBank);
		}
	}

	@:bind(tab, UIEvent.CHANGE)
	private function showGift(e:MouseEvent) {
		if (tab.selectedPage == text_editor_tab) {
			if (shownQuestion != null) {
				text_editor.text = shownQuestion.toGift();
			}
		}
	}

	@:bind(tab, UIEvent.CHANGE)
	private function showPlayCard(e:MouseEvent) {
		if (tab.selectedPage == play_card_tab) {
			play_card.removeAllComponents();
			var shownCard = UIUtils.createCard(shownQuestion);
			if (shownCard != null)
				play_card.addComponent(shownCard);
		}
	}

	@:bind(lv, UIEvent.CHANGE)
	private function selectItem(e:MouseEvent) {
		properties.show();
		//resetTagsDropdown();
		feedback_editor.reset();
		if (Type.typeof(lv.selectedItem) == TNull)
			return;
		if (Type.typeof(lv.selectedItem.question) == TNull)
			return;
		MainEditor.shownQuestion = lv.selectedItem.question;
		play_card.removeAllComponents();
		tagsbox.reset();


		showGift(e);
		showPlayCard(e);

		origin.text = lv.selectedItem.origin;

		feedback_editor.show();
		tagsbox.show();

		editor.removeAllComponents();

		if (lv.selectedItem.question.type == null) {
			editor.addComponent(new editor.QuestionTypeGrid());
			tagsbox.hide();
			feedback_editor.hide();
			tab.pageIndex = 0;
			return;
		}

		
		var shownCard:editor.ICardEditor = switch (lv.selectedItem.question.type) {
			case TrueFalse:
				new editor.TrueFalseEditor();
			case Multichoice:
				new editor.MultipleChoiceEditor();
			case Short:
				new editor.ShortEditor();
			case Description:
				new editor.DescriptionEditor();
			case Essay:
				new editor.EssayEditor();
			case Numerical:
				new editor.NumericalEditor();
			case MultiAnswers:
				new editor.MultiAnswersEditor();
			case Match:
				new editor.MatchEditor();
			case MissingWords:
				new editor.MissingWordsEditor();
		}

		editor.addComponent(cast( shownCard, Component));
		if (MainEditor.shownQuestion != null ) shownCard.setToQuestion(MainEditor.shownQuestion);
	}
}
