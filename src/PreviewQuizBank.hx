import haxe.ui.core.Component;
import quizparser.QuizBank;
import haxe.ui.containers.ScrollView;
import haxe.ui.containers.VBox;
import haxe.ui.containers.ScrollView;

class PreviewQuizBank extends ScrollView {

    public function new() {
        super();
    }

    public function showQuestionBank(qb:QuizBank) {

        if (findComponent("vbox_cards", Component)!= null) {
            this.removeComponent(findComponent("vbox_cards", Component));
        }
        
        var vbox = new VBox();
        vbox.id = "vbox_cards";
        vbox.percentWidth = 100;
        this.addComponent(vbox);

        for ( q in qb.questions) {
            var card = UIUtils.createCard(q);
            card.disabled = true;
            card.customStyle.minWidth = 300;
            card.removeInteractivity();
            card.showCorrect();

            
            vbox.addComponent(card);

        }
        


    }

}