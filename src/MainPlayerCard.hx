import cards.PlayCard;
import quizparser.Question;
import haxe.ui.containers.VBox;
import quizparser.QuizBank;
import quizparser.QuizBankPlayer;

import haxe.ui.events.MouseEvent;

@:build(haxe.ui.ComponentBuilder.build("assets/main-player-card.xml"))
class MainPlayerCard extends VBox {
	var qb:QuizBank = null;
	var qbp:QuizBankPlayer = null;
	var params:Dynamic = null;

	public function new(qb:QuizBank, params:Dynamic) {
		super();
		this.qb = qb;
		progress.pos = 0;
		this.params = params;
		qbp = new QuizBankPlayer(qb);
		qbp.shuffle = (params.order == "random");
		qbp.init();

		progress.max = qbp.questions.length;

		var attempt = qbp.getActualQuestionAttempt();
		MainEditor.shownQuestion = attempt.question;
		showPlayCard(attempt.question, params);
		previous_card.disabled = true;
		trace(qbp.isLastQuestion());
		if (qbp.isLastQuestion()) {
			next_card.disabled = true;
		}
	}

	private function showPlayCard(shownQuestion:Question, params:Dynamic) {
		play_card.removeAllComponents();
		var attempt = qbp.getActualQuestionAttempt();
		var shownCard:PlayCard = UIUtils.createCard(shownQuestion, attempt);
		shownCard.feedbackMode = params.feedback;

		
		if (shownQuestion == null)
			return;

		play_card.addComponent(shownCard);
		if (attempt.isValidated) {
			shownCard.removeInteractivity();
			shownCard.showCorrect();		
		}
		shownCard.onFinish = function f() {
			score.text = "" + qbp.getCurrentScore();
			++progress.pos;
			qbp.goToNextQuestion();
			
			var attempt = qbp.getActualQuestionAttempt();
			
			
			if (attempt == null) {
				next_card.disabled = true;
				switch (shownCard.feedbackMode) {
					case feedback_answer_end | feedback_question_end:
						back_main.show();
					case feedback_quizbank_end:
						cards_review.show();
				}

				
				return;
			}
				

			if (qbp.isLastQuestion()) {
				next_card.disabled = true;
			}
			previous_card.disabled = false;
			MainEditor.shownQuestion = attempt.question;
			showPlayCard(attempt.question, params);
		}
	}

	@:bind(cards_review, MouseEvent.CLICK)
	private function showReview(e) {
		var review = new CorrectQuizBank();
		review.percentHeight = 100;
		review.percentWidth  = 100;
		
		Main.mainContainer.removeComponent(this);
		Main.mainContainer.addComponent(review);
		review.showQuestionBank(qb, qbp);
		
	}

	@:bind(previous_card, MouseEvent.CLICK)
	private function goPreviousCard(e) {
		qbp.goToPreviousQuestion();
		var attempt = qbp.getActualQuestionAttempt();
		if (attempt == null)
			return;

		if (qbp.isFirstQuestion()) {
			previous_card.disabled = true;
		}

		next_card.disabled = false;
		MainEditor.shownQuestion = attempt.question;
		
		showPlayCard(attempt.question, params);
	}

	@:bind(next_card, MouseEvent.CLICK)
	private function goNextCard(e) {
			qbp.goToNextQuestion();
			var attempt = qbp.getActualQuestionAttempt();
			previous_card.disabled = false;
			MainEditor.shownQuestion = attempt.question;
			if (qbp.isLastQuestion()) {
				next_card.disabled = true;
			}
			
			showPlayCard(attempt.question, params);
		
	}

	@:bind(back_main, MouseEvent.CLICK)
	private function backToMain(e) {
		var player = Main.mainContainer.findComponent(MainPlayer);
		player.show();
		Main.mainContainer.removeComponent(this);

	}

}
