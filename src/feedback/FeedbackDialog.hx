package feedback;

import quizparser.Answer;
import haxe.ui.containers.dialogs.Dialog;

@:build(haxe.ui.ComponentBuilder.build("assets/feedback-dialog.xml"))
class FeedBackDialog extends Dialog {
    public function new(?a:Answer, ?isCorrect:Bool) {
        super();
        title= "";
        buttons = "{{INTERESTING}}";
        addClass("custom-dialog-footer");
        if (a!= null) {
            feedback.text = a.feedback;
            score.text = a.fraction + "% !";
        }
        if (isCorrect!= null) {
            if (isCorrect) feedback.text = "Bien ! :)";
            else {
                feedback.text = ":(";
            }
        }
    }
}