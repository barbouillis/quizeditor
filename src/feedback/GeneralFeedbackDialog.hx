package feedback;

import haxe.ui.components.Label;
import quizparser.Question;
import haxe.ui.containers.Box;
import haxe.ui.containers.dialogs.Dialog;
import haxe.ui.events.MouseEvent;
import quizparser.QuizBankPlayer.QuestionAttempt;

@:build(haxe.ui.ComponentBuilder.build("assets/feedback-dialog.xml"))
class GeneralFeedbackDialog extends Dialog {
    public function new(q:Question, qa:QuestionAttempt = null) {
        super();
        title = "General Feedback";
        buttons = "{{INTERESTING}}";
        feedback.text = q.generalFeedback;
        addClass("custom-dialog-footer");
        //score.text = a.fraction + "% !";

        // TODO total score
        // TODO answer
        // TODO answerd + feedbacks
        if (qa != null) {
            for (a in qa.answers) {
                if (a.answered) {
                    var label = new Label();
                    label.text = a.answer.text;
                    grid.addComponent(label);
                    var label = new Label();
                    label.text = a.answer.feedback;
                    grid.addComponent(label);
                }
                trace(a);
            }
        }
    }
}