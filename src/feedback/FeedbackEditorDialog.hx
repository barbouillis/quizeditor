package feedback;

import quizparser.Answer;
import haxe.ui.containers.dialogs.Dialog;
import haxe.ui.containers.dialogs.Dialog.DialogButton;

@:xml('
<dialog width="400" height="300">
<textarea id="textarea" width="100%" height="100%"/>
</dialog>
')
class FeedbackEditorDialog extends Dialog {
    public function new(title:String) {
        super();
        buttons = DialogButton.APPLY | DialogButton.CANCEL;
    }
}