
#if sys
import sys.io.File;
#end
import quizparser.QuizBank;
import quizparser.Question;
import haxe.ui.components.Button;
import haxe.ui.containers.VBox;
import haxe.ui.events.UIEvent;
import haxe.ui.events.MouseEvent;

@:build(haxe.ui.ComponentBuilder.build("assets/main-player.xml"))
class MainPlayer extends VBox {

    public var quizbanks:Map<String,QuizBank> = new Map();

    public var shownQuizbanks:Array<QuizBank> =[];

    public var selectedQuizbanks:Array<QuizBank> = [];

    public function new() {
        super();
    }

    #if sys
    @:bind(this, UIEvent.READY)
	private function initLoad(e) {
        for (k in quizbanks.keys()) {
            return;
        }
		if (Main.openedPath != null) {
			var file = File.getContent(Main.openedPath);
			addQuizbank(file, Main.openedPath);
		}
	}
    #end

    @:bind(this, UIEvent.SHOWN)
	private function setupDrop(e) {
		UIUtils.setupDrop(player_box, addQuizbank );
	} 

    @:bind(add_file, MouseEvent.CLICK)
    private function addFile(e) {
        haxe.ui.containers.dialogs.Dialogs.openFile(function(b, files) {
			if (b == haxe.ui.containers.dialogs.Dialog.DialogButton.OK) {
                addQuizbank(files[0].text, files[0].name);
			}
		}, {
			extensions: [{extension: "txt,gift", label: "Text Files"},],
			readContents: true
		});
    }

    


    @:bind(start, MouseEvent.CLICK)
    private function start_player(e) {

        if (selectedQuizbanks.length  == 0) {
            return;
        }

       var qbs = selectedQuizbanks[0]; 
       for (qb in selectedQuizbanks.slice(1)) {
           qbs.concat(qb);

       }

        var params = {
            order : ordered.selectedOption.id,
            feedback : feedback_answer_end.selectedOption.id
        };


        //Main.mainContainer.removeComponent(this);
        this.hide();
      
       var player = new MainPlayerCard(qbs, params);
       player.percentWidth = 100;
       player.percentHeight= 100;
        Main.mainContainer.addComponent( player);


    }

    private function addQuizbank(s:String, name:String) {
        var qb = new QuizBank();
        qb.loadBankFromString(s);
        quizbanks[name] = qb;
        switch (ordering.selectedItem.data) {
            case "tags":
                orderByTags([qb]);
            case "quizbank":
                addFileCard(name, qb);
        }
        drop_indication.hidden = true;
    }


    private function addTagCard(name:String,qb:QuizBank) {
        var button = card_grid.findComponent(name, Button);
        if ( button == null) {
            var button = new Button();
            button.text = name + " " + qb.questions.length;
            button.id = name;
            button.toggle = true;
            button.onChange = function (_) {
                if (button.selected) { 
                    selectedQuizbanks.push(qb);
                    start.disabled = false;
                }
                else {
                    selectedQuizbanks.remove(qb);
                    if (selectedQuizbanks.length == 0) start.disabled = true;
                }
            }
            card_grid.addComponent(button);
        }
        else {
            button.text = name + " " + qb.questions.length;
        }
        

    }

    private function addFileCard(name:String,qb:QuizBank) {
        var button = new Button();
        button.text = name + qb.questions.length;
        button.toggle = true;
        button.onChange = function (_) {
                if (button.selected){
                    selectedQuizbanks.push(qb);
                    start.disabled = false;
                }
                else {
                    selectedQuizbanks.remove(qb);
                    if (selectedQuizbanks.length == 0) start.disabled = true;
                }
            }
        card_grid.addComponent(button);

    }

    @:bind(ordering, UIEvent.CHANGE)
    private function onOrdering(e) {
        card_grid.removeAllComponents();

        switch (ordering.selectedItem.data) {
            case "tags":
                orderByTags([for (key in quizbanks) key] );
            case "quizbank":
                orderByBanks();

        }

    }

    private function orderByBanks() {
        for (name => qb in quizbanks) {
            addFileCard(name, qb);
        }

    }

    private function orderByTags(qbs:Array<QuizBank>) {
        
        var tags:Array<String> = [];
        var map:Map<String, Array<Question>>  = new Map();

        
        for (q in quizbanks) {
            var extractTaggedQuestions = q.extractTaggedQuestions();
            for (t in extractTaggedQuestions.keys()) {
                if (!tags.contains(t)) tags.push(t);
            }
            for (k => a in  extractTaggedQuestions) {
                if (!map.exists(k)) map[k] = a
                else {
                    map[k] = map[k].concat(a);
                }
            }
        }

        for ( t in tags ) {
            var qb = new QuizBank();
            qb.questions = map[t];
            addTagCard(t, qb);
            
        }
    }
}