package cards;

import quizparser.Question;
import haxe.ui.containers.Card;
import haxe.ui.events.MouseEvent;

@:build(haxe.ui.ComponentBuilder.build("assets/description-card.xml"))
class DescriptionCard extends PlayCard {
    public function new() {
        super();     
    }

    @:bind(validate, MouseEvent.CLICK)
    private function validate_description(e)  {
        showGeneralFeedback();
    }

    override public function setToQuestion(q:Question) {
        this.question = q;
        text = q.title;
        question_text.text = q.text;
    }
}