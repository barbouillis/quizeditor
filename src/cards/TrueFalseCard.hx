package cards;

import quizparser.Question;
import quizparser.Answer.AnswerTrueFalse;
import haxe.ui.events.MouseEvent;
import feedback.FeedbackDialog;

@:build(haxe.ui.ComponentBuilder.build("assets/truefalse-card.xml"))
class TrueFalseCard extends PlayCard {
	public function new() {
		super();
	}

	override public function setToQuestion(q:Question) {
		text = q.title;
		question_text.text = q.text;
		this.question = q;
	}

	@:bind(button_true, MouseEvent.CLICK)
	function onButtonTrue(e) {
		switch (feedbackMode) {
			case(feedback_answer_end | feedback_question_end):
				var dialog = new FeedBackDialog(answerFor(true));
				if (questionAttempt != null) {
					questionAttempt.addAnswer(answerFor(true));
					questionAttempt.setAnswer(answerFor(true), true);
					questionAttempt.isValidated = true;
				}
				removeInteractivity();
				dialog.onDialogClosed = function(e) {
					showGeneralFeedback();
				}
				dialog.showDialog();
			case feedback_quizbank_end:
				onFinish();
		}
	}

	@:bind(button_false, MouseEvent.CLICK)
	function onButtonFalse(e) {
		switch (feedbackMode) {
			case(feedback_answer_end | feedback_question_end):
				var dialog = new FeedBackDialog(answerFor(false));
				if (questionAttempt != null) {
					questionAttempt.addAnswer(answerFor(false));
					questionAttempt.setAnswer(answerFor(false), true);
					questionAttempt.isValidated = true;
				}
				removeInteractivity();
				dialog.onDialogClosed = function(e) {
					showGeneralFeedback();
				}
				dialog.showDialog();
			case feedback_quizbank_end:
				onFinish();
		}
	}

	function answerFor(b:Bool):AnswerTrueFalse {
		var result = cast(question.answers.list[0], AnswerTrueFalse).result;
		if (result == b)
			return cast question.answers.list[0];
		return cast question.answers.list[1];
	}

	function feedbackFor(b:Bool):String {
		var result = cast(question.answers.list[0], AnswerTrueFalse).result;

		if (result == b)
			return question.answers.list[0].feedback;
		if (question.answers.list.length > 1)
			return question.answers.list[1].feedback;
		return "";
	}

	override public function showCorrect() {
		if (questionAttempt != null) {
			for (a in questionAttempt.answers.slice(0, 1)) {
				if (a.answer.fraction == 100) {
					if (a.answered && cast(a.answer, AnswerTrueFalse).result) {
						button_true.addClass("answered");
					} else {
						button_false.addClass("answered");
					}
				} else {
					if (a.answered) {
						button_false.addClass("answered");
					} else {
						button_true.addClass("answered");
					}
				}
			}
		}
		if (cast(question.answers.list[0], AnswerTrueFalse).result) {
			button_true.addClass("correct");
		} else {
			button_false.addClass("correct");
		}
	}
}
