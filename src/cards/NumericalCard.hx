package cards;

import quizparser.Answer.AnswerNumerical;
import quizparser.Question;
import quizparser.Question;
import quizparser.Question;
import haxe.ui.containers.Card;
import feedback.FeedbackDialog;

@:build(haxe.ui.ComponentBuilder.build("assets/numerical-card.xml"))
class NumericalCard extends PlayCard {
	public function new() {
		super();
	}

	override public function setToQuestion(q:Question) {
		this.question = q;
		text = q.title;
		question_text.text = q.text;
		question_checkanswer.onClick = function(e) {
			var answer = q.answers.checkAnswer(question_answerfield.pos);
			if (questionAttempt != null) {
				questionAttempt.setAnswer(answer, question_answerfield.pos);
				questionAttempt.isValidated = true;
			}
			if (answer != null) {
				var dialog = new FeedBackDialog(answer);
				dialog.onDialogClosed = function(e) {
					showGeneralFeedback();
				}
				dialog.showDialog();
			} else {
				var dialog = new FeedBackDialog(false);
				dialog.onDialogClosed = function(e) {
					showGeneralFeedback();
				}
				dialog.showDialog();
			}
		}
	}

	override public function showCorrect() {
		for (a in question.answers.list) {
			if (a.fraction == 100) {
				question_answerfield.min = cast(a, AnswerNumerical).value;
				question_answerfield.max = cast(a, AnswerNumerical).value;
				question_answerfield.pos = cast(a, AnswerNumerical).value;
				question_answerfield.addClass("correct");
			}
		}

		if (questionAttempt != null) {
			for (a in questionAttempt.answers) {
				if (a.answer == null) {
					question_answerfield.removeClass("correct");
					question_answerfield.addClass("incorrect");
				}
				question_answerfield.pos = a.answered;
			}
		}
	}
}
