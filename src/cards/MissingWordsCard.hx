package cards;

import haxe.ui.events.UIEvent;
import quizparser.Answer;
import quizparser.Question;
import haxe.ui.containers.HBox;
import haxe.ui.containers.Card;
import haxe.ui.components.Label;
import haxe.ui.components.TextField;
import haxe.ui.components.OptionBox;
import haxe.ui.components.Button;
import feedback.GeneralFeedbackDialog;
import feedback.FeedbackDialog;

@:build(haxe.ui.ComponentBuilder.build("assets/missingwords-card.xml"))
class MissingWordsCard extends PlayCard {
	public var answersToComponents:Map<Answer, HBox> = new Map();

	public var question_answerfield:TextField = null;

	public function new() {
		super();
	}

	override public function setToQuestion(q:Question) {
		this.question = q;
		text = q.title;

		if (q.answers.areAllCorrect()) {
			var hBox = new HBox();

			hBox.continuous = true;
			addComponent(hBox);
			var label = new Label();
			label.text = q.text;
			hBox.addComponent(label);
			question_answerfield = new TextField();
			question_answerfield.placeholder = "{{ENTER_ANSWER}}";
			
			hBox.addComponent(question_answerfield);
			var label = new Label();
			label.text = q.textAfter;
			hBox.addComponent(label);

			var button = new Button();
			button.text = "{{SUBMIT_ANSWER}}"; // TODO
			addComponent(button);
			function validate() {
				var goodAnswer = false;
				for (a in q.answers.list) {
					if (a.text == question_answerfield.text) {
						var dialog = new FeedBackDialog(a);
						if (questionAttempt != null) {
							questionAttempt.addAnswer(a);
							questionAttempt.setAnswer(a, a.text);
							questionAttempt.isValidated = true;
						}
						goodAnswer = true;
						dialog.onDialogClosed = function f(e) {
							showGeneralFeedback();
						};
						dialog.showDialog();
					}

					break;
				}
				if (!goodAnswer) {
					if (questionAttempt != null) {
						questionAttempt.addAnswer(null);
						questionAttempt.setAnswer(null, question_answerfield.text);
						questionAttempt.isValidated = true;
					}
					showGeneralFeedback();
				}
			}

			button.onClick = function(e) {
				validate();
			}

			question_answerfield.registerEvent(UIEvent.SUBMIT, function f(e) {
				validate();
			});
			
		} else {
			question_text.text = q.text + " ___ " + q.textAfter;

			for (a in q.answers.list) {
				var hBox = new HBox();

				var button = new Button();
				button.componentGroup =  "missing_word" + q.id;
				button.id = "select_button";
				button.toggle = true;
				hBox.addComponent(button);

				var label = new Label();
				label.text = a.text;
				label.id = "answer_label";
				hBox.addComponent(label);

				this.addComponent(hBox);

				answersToComponents[a] = hBox;
				// TODO should work with onChange
				button.onClick = function(e) {
					var result = a.fraction == 100;
					// haxe.ui.containers.dialogs.Dialogs.messageBox(result ? "Correct" : 'Incorrect', 'Info', 'info');
					trace("aaa");
					for (a in q.answers.list) {
						if (a.text == label.text) {
							trace("aaa");
							if (questionAttempt != null) {
								trace("aaa");
								questionAttempt.addAnswer(a);
								trace("aaa");
								questionAttempt.setAnswer(a, a.text);
								trace("aaa");
								questionAttempt.isValidated = true;
							}
						}
					}

					var dialog = new FeedBackDialog(a);
					dialog.onDialogClosed = function f(e) {
						showGeneralFeedback();
					};
					dialog.showDialog();
				}
			}
		}
	}

	override public function showCorrect() {
		//findComponent(Button).hide();;
		if (question.answers.areAllCorrect()) {
			for (a in question.answers.list) {
				if (a.fraction == 100) {
					question_answerfield.text = a.text;
					question_answerfield.addClass("correct");
					break;
				}
			}
		} else {
			for (a in question.answers.list) {
				if (a.fraction == 100) {
					answersToComponents[a].findComponent("select_button", Button).selected = true;
					answersToComponents[a].addClass("correct");
					answersToComponents[a].show();
				}
				answersToComponents[a].findComponent("select_button", Button).text = "" + a.fraction;
			}
		}

		if (questionAttempt != null) {
			if (question.answers.areAllCorrect()) {
				for (a in questionAttempt.answers) {
					if (a.answer == null) {
						question_answerfield.removeClass("correct");
						question_answerfield.addClass("incorrect");
					}
					question_answerfield.text = a.answered;
				}
			}
			else {
				for (a in questionAttempt.answers) {

					trace(a);
					trace(a.answer) ;
					//if (a.answer )
						answersToComponents[a.answer].findComponent("answer_label", Label).htmlText =  "<b>" +
						answersToComponents[a.answer].findComponent("answer_label", Label).text +"</b>";
						//customStyle.fontBold = true;
				}
			}
		}
	}
}
