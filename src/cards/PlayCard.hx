package cards;

import quizparser.Question;
import quizparser.QuizBankPlayer.QuestionAttempt;
import haxe.ui.containers.Card;

enum abstract Feedback(String) {
    var feedback_answer_end;
    var feedback_question_end;
    var feedback_quizbank_end;
}

class PlayCard extends Card {

    public var onFinish:Void->Void;
    public var questionAttempt:QuestionAttempt;
    public var question:Question;

    public var feedbackMode:Feedback = feedback_answer_end;


    function showGeneralFeedback() {

        if (MainEditor.shownQuestion.generalFeedback != "") {
            var dialog = new feedback.GeneralFeedbackDialog(MainEditor.shownQuestion);
            dialog.onDialogClosed = function (e) {
            if (onFinish != null) onFinish();
            }
            dialog.showDialog();
        }
        else {
            if (onFinish != null) onFinish();
        }
    }

    function showCombinedGeneralFeedback() {
        var dialog = new feedback.GeneralFeedbackDialog(MainEditor.shownQuestion, questionAttempt);
            dialog.onDialogClosed = function (e) {
            if (onFinish != null) onFinish();
            }
            dialog.showDialog();

        if (questionAttempt != null) {
			for (a in questionAttempt.answers) {
                trace(a);
            }
        }
    }

    public function showCorrect() {

    }

    public function removeInteractivity() {
        addClass("readmode");
        this.disableInteractivity(true);
        //this.disabled = true;
        walkComponents(function f(c) {c.disabled = true; return true;});

       /* for ( c in this.wa) {
            c.disabled = true;
        }*/
        //this.disableInteractivity(true);
    }

    public function setToQuestion(q:Question) {
        this.question = q;
    }
}