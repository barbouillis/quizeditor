package cards;

import haxe.ui.Toolkit;
import quizparser.Answer;
import quizparser.Question;
import quizparser.Question;
import quizparser.Answer.AnswerMatch;
import haxe.ui.components.DropDown;
import haxe.ui.containers.Card;
import haxe.ui.components.Label;
import feedback.FeedbackDialog;

@:build(haxe.ui.ComponentBuilder.build("assets/match-card.xml"))
class MatchCard extends PlayCard {
	public var leftDropdowns:Array<DropDown> = [];
	public var answersToDropdowns:Map<AnswerMatch, DropDown> = new Map();

	public function new() {
		super();
		pairs.columns = 2;
		this.customStyle.minWidth = 300;
	}

	override public function showCorrect() {
		trace("show correct");
		for (a => d in answersToDropdowns) {
			d.selectedItem = {text: a.textRight};
		}

		if (questionAttempt != null) {
			for (aa in questionAttempt.answers) {
				var answer = cast(aa.answer, AnswerMatch);
				var dropdown = answersToDropdowns.get(answer);

				if (dropdown.selectedItem.text != aa.answered) {
					var selectedItem = {text: aa.answered};
					dropdown.selectedItem = selectedItem;
					dropdown.addClass("incorrect");
					dropdown.registerEvent(haxe.ui.events.UIEvent.READY, function(e) {
						answersToDropdowns[answer].selectedItem = selectedItem;
					});
				} else {
					dropdown.addClass("correct");
				}
			}
		}
		trace("show correct");
	}

	override public function setToQuestion(q:Question) {
		this.question = q;
		text = q.title;
		question_text.text = q.text;

		var list:Array<String> = [];
		for (a in q.answers.list) {
			var aa:AnswerMatch = cast a;
			if (!list.contains(aa.textRight))
				list.push(aa.textRight);
		}

		function makeDropDown() {
			var dropdown = new DropDown();
			dropdown.dataSource.allowCallbacks = false;
			for (a in list) {
				dropdown.dataSource.add({text: a});
			}
			dropdown.dataSource.allowCallbacks = true;
			dropdown.text = "select ...";
			return dropdown;
		}

		for (a in q.answers.list) {
			var aa:quizparser.Answer.AnswerMatch = cast a;

			if (aa.textLeft == "")
				continue;

			var label = new Label();
			label.text = aa.textLeft;

			pairs.addComponent(label);

			var dropdown2 = makeDropDown();
			answersToDropdowns[cast a] = dropdown2;
			dropdown2.dropdownWidth = 300;
			dropdown2.customStyle.minWidth = 200;
			dropdown2.customStyle.maxPercentWidth = 100;
			leftDropdowns.push(dropdown2);

			dropdown2.onChange = function(e) {
				if (questionAttempt.isValidated)
					return;

				if (dropdown2.disabled)
					return;
				leftDropdowns.remove(dropdown2);
				for (a in q.answers.list) {
					var aa:AnswerMatch = cast aa;
					if (aa.textLeft == label.text) {
						if (dropdown2.text == aa.textRight) {
							if (questionAttempt != null) {
								questionAttempt.setAnswer(a, dropdown2.text);
							}

							var dialog = new FeedBackDialog(a);
							dialog.showDialog();
						} else {
							if (questionAttempt != null) {
								questionAttempt.setAnswer(a, dropdown2.text);
							}
						}

						break;
					}
				}

				if (leftDropdowns.length == 0) {
					// if (questionAttempt != null) questionAttempt.addAnswer(answerFor(true));
					// questionAttempt.setAnswer(answerFor(true), true);

					

					if (questionAttempt != null)
						questionAttempt.isValidated = true;
					removeInteractivity();
					Toolkit.callLater(function f() {showGeneralFeedback(); });
					
				}
			}

			pairs.addComponent(dropdown2);
		}
	}
}
