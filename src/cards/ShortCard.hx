package cards;

import quizparser.Question;
import haxe.ui.containers.Card;
import haxe.ui.events.MouseEvent;

import feedback.FeedbackDialog.FeedBackDialog;

@:build(haxe.ui.ComponentBuilder.build("assets/short-card.xml"))
class ShortCard extends PlayCard {
    public function new() {
        super();
    }

    @:bind(validate, MouseEvent.CLICK)
    private function check_answer(e) {
        var found = false;
        for (a in question.answers.list) {
            if (a.text == question_answerfield.text) {
                if (questionAttempt != null){ questionAttempt.addAnswer(a);
                questionAttempt.setAnswer(a, a.text);
                questionAttempt.isValidated = true;
                }
                removeInteractivity();
                found = true;
                var  dialog = new FeedBackDialog(a);
                dialog.onDialogClosed = function f(e){ showGeneralFeedback(); };
                dialog.showDialog();
                break;
            }
            else {
                questionAttempt.setAnswer(a, question_answerfield.text);
                questionAttempt.isValidated = true;
            }
        }

        if (!found) {
            var  dialog = new FeedBackDialog(null, false);
                
                dialog.onDialogClosed = function f(e){ showGeneralFeedback(); };
                dialog.showDialog();
        }
    }

    override public function setToQuestion(q:Question) {
        text = q.title;
        question_text.text = q.text;
        this.question = q;
    }

    override public function showCorrect() {
        

		for (a in question.answers.list) {
			if (a.fraction == 100) {
                question_answerfield.text = a.text;
				question_answerfield.addClass("correct");
                break;
			}
		}

	}
}