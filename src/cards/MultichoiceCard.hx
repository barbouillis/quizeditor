package cards;

import quizparser.Question;
import quizparser.Answer;
import haxe.ui.containers.HBox;
import haxe.ui.components.Image;
import haxe.ui.components.Label;
import haxe.ui.core.ItemRenderer;
import feedback.FeedbackDialog;
import haxe.ui.containers.Card;
import haxe.ui.components.Button;
import haxe.ui.core.Component;

@:build(haxe.ui.ComponentBuilder.build("assets/multichoice-card.xml"))
class MultichoiceCard extends PlayCard {
	public var answersToComponents:Map<Answer, HBox> = new Map();

	public function new() {
		super();
	}

	override public function setToQuestion(q:Question) {
		this.question = q;
		// super.setToQuestion(q);
		text = q.title;
		question_text.text = q.text;
		for (a in q.answers.list) {
			var optionBox = new HBox();
			var button    = new Button();
			//var itemRenderer = new ItemRenderer();
			//var label = new Label();
			//label.text = a.text;
			//label.id = "check-label";
			var image = new Image();
			image.id = "check";

			button.text = a.text;
			button.id = "check-label";

			//var hbox = new HBox();
			//hbox.addComponent(label);
			optionBox.addComponent(button);
			optionBox.addComponent(image);
			//itemRenderer.addComponent(hbox);

			//optionBox.addComponent(itemRenderer);
			options.addComponent(optionBox);

			answersToComponents[a] = optionBox;

			button.onClick = function(e) {
				showCorrectAnswers(optionBox);

				var dialog = new FeedBackDialog(a);
				if (questionAttempt != null) {
					questionAttempt.addAnswer(a);

				questionAttempt.setAnswer(a, true);
                questionAttempt.isValidated = true;
				}
				
				dialog.onDialogClosed = function f(e) {
					showGeneralFeedback();
				};

				dialog.showDialog();
			}
		}
	}

	override public function showCorrect() {
		if (questionAttempt != null) {
		for (a in questionAttempt.answers) {
			if (a.answered) {
				answersToComponents[a.answer].findComponent(Button).addClass("answered");
			}
		 }
		}
		for (a in question.answers.list) {
			if (a.fraction == 100) {
                answersToComponents[a].findComponent(Button).selected = true;
				answersToComponents[a].findComponent(Button).addClass("correct");
				answersToComponents[a].show();
				//if (answersToComponents[a].findComponent("check-label", Component) != null) answersToComponents[a].findComponent("check-label", Component).invalidateComponent();
				//if (answersToComponents[a].findComponent("check", Component) != null)answersToComponents[a].findComponent("check", Component).invalidateComponent();
			}
		}
	}

	public function showCorrectAnswers(optionBox:HBox) {
		optionBox.addClass("selected-answer");

		

		for (a in MainEditor.shownQuestion.answers.list) {
			if (a.fraction == 100) {
				answersToComponents[a].addClass("correct");
				answersToComponents[a].show();
				answersToComponents[a].findComponent("check-label", Component).invalidateComponent();
				answersToComponents[a].findComponent("check", Component).invalidateComponent();
			} else if (answersToComponents[a] == optionBox) {
				optionBox.addClass("selected-answer-incorrect");
				optionBox.show();
				optionBox.findComponent("check", Component).invalidateComponent();
			}
		}

		optionBox.show();
		optionBox.findComponent("check", Component).invalidateComponent();
		optionBox.findComponent("check-label", Component).invalidateComponent();

		removeInteractivity();
	}
}
