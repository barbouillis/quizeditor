package cards;

import quizparser.Question;
import quizparser.Answer;
import haxe.ui.containers.Card;
import haxe.ui.components.CheckBox;

import haxe.ui.containers.HBox;
import haxe.ui.components.Image;
import haxe.ui.components.Label;
import haxe.ui.core.ItemRenderer;

import haxe.ui.events.MouseEvent;

import feedback.FeedbackDialog.FeedBackDialog;

@:build(haxe.ui.ComponentBuilder.build("assets/multianswers-card.xml"))
class MultiAnswersCard extends PlayCard {
    public var answersToCheckBox:Map<Answer, CheckBox> = new Map();

    public function new() {
        super();
    }


    @:bind(validate, MouseEvent.CLICK)
    private function check_answer(e) {
        trace(feedbackMode);
        if (feedbackMode == feedback_answer_end) {
            showGeneralFeedback();
        }
        else if (feedbackMode == feedback_question_end) {
            showCombinedGeneralFeedback();
        }
        else {
            if (onFinish != null) onFinish();
        }
    }

    override public function setToQuestion(q:Question) {
        this.question = q;
        text = q.title;
        question_text.text = q.text;
        for (a in q.answers.list) {
            var optionBox = new CheckBox();
            optionBox.text = a.text;

            /*
            var itemRenderer = new ItemRenderer();
			var label = new Label();
			label.text = a.text;
			label.id = "check-label";
			var image = new Image();
			image.id = "check";

			var hbox = new HBox();
			hbox.addComponent(label);
			hbox.addComponent(image);
			itemRenderer.addComponent(hbox);

			optionBox.addComponent(itemRenderer);*/
			options.addComponent(optionBox);
            answersToCheckBox[a] = optionBox;
            optionBox.onChange = function(e) {
                if (questionAttempt != null) {
					//questionAttempt.addAnswer(a);

                    questionAttempt.setAnswer(a, true);
                    questionAttempt.isValidated = true;
				}
                if (feedbackMode == feedback_answer_end) {
                    var  dialog = new FeedBackDialog(a);
                    showPartialCorrect(a);
                    dialog.showDialog();
                    optionBox.disabled = true;
                }
            }
        }
    }

    public function showPartialCorrect(a:Answer) {
        answersToCheckBox[a].addClass("answered");
        if (a.fraction > 0) {
            answersToCheckBox[a].addClass("correct");
        }
        else {
            answersToCheckBox[a].addClass("incorrect");
        }
    }

    override public function showCorrect() {

        validate.hide();

        var answers = question.answers.list.copy();
        answers.sort((a, b) -> b.fraction - a.fraction);
        var totalFraction = 0;
		for (a in answers) {
            if (totalFraction < 100 ) {
                if (a.fraction > 0 ) {
                    answersToCheckBox[a].selected = true;
                    answersToCheckBox[a].addClass("correct");
                    answersToCheckBox[a].show();
                    totalFraction += a.fraction;
                }
            }
            else {
                break;
            }   
		}

        if (questionAttempt != null) {
			for (a in questionAttempt.answers) {
				if (a.answered == true) {
                    answersToCheckBox[a.answer].addClass("selected-answer");
				}

			}
		}
	}
              
}