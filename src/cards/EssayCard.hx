package cards;

import quizparser.Question;
import haxe.ui.containers.Card;
import haxe.ui.events.MouseEvent;

@:build(haxe.ui.ComponentBuilder.build("assets/essay-card.xml"))
class EssayCard extends PlayCard {
    public function new() {
        super();
    }

    @:bind(validate, MouseEvent.CLICK)
    private function validate_essay(e)  {
        if(onFinish != null) onFinish();
    }

    override public function setToQuestion(q:Question) {
        this.question = q;
        text = q.title;
        question_text.text = q.text;
    }
}