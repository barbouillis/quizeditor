import cards.PlayCard;
import quizparser.Question;

#if js
import js.html.FileList;
import js.html.FileReader;
#else
import haxe.io.Path;
import sys.io.File;
#end


class UIUtils {

	static var nowBindFunction = null;

    static public function setupDrop(target:haxe.ui.core.Component, ?onLoad:String->String->Void) {

		/*
		var oldTrace = haxe.Log.trace;
		haxe.Log.trace = function(v:Dynamic, ?pos: haxe.PosInfos) {
			oldTrace("[" + Date.now().toString() + "] " + Std.string(v), pos);
		};*/

		#if js
		function preventDefaults(e) {
			e.preventDefault();
			e.stopPropagation();
		}

		function handleDrop(e) {
			var dt = e.dataTransfer;
			var files:FileList = dt.files;

			var fileReader = new FileReader();
			fileReader.readAsText(files.item(0));

			fileReader.onloadend = function() {
                onLoad(fileReader.result, files.item(0).name);
			}

			// handleFiles(files)
		}
		target.element.addEventListener('drop', preventDefaults, false);
		target.element.addEventListener('dragenter', preventDefaults, false);
		target.element.addEventListener('dragover', preventDefaults, false);
		target.element.addEventListener('drageleave', preventDefaults, false);

		target.element.addEventListener('drop', handleDrop, false);

		#elseif haxeui_hxwidgets 
		if (nowBindFunction == null)  haxe.ui.core.Screen.instance.frame.dragAcceptFiles(true);

		var bindFunction = function(e:hx.widgets.Event) {
			var dfe = e.convertTo(hx.widgets.DropFilesEvent);
			var name = Path.withoutDirectory(dfe.files[0]);
			name = Path.withoutExtension(name);
			onLoad(File.getContent(dfe.files[0]), name);
		}

		if (nowBindFunction != null) haxe.ui.core.Screen.instance.frame.unbind(hx.widgets.EventType.DROP_FILES, nowBindFunction );
		haxe.ui.core.Screen.instance.frame.bind(hx.widgets.EventType.DROP_FILES, bindFunction );

		nowBindFunction = bindFunction;

		#end
	}
	static public function createCard(q:Question, qA:quizparser.QuizBankPlayer.QuestionAttempt = null):PlayCard {
		if (q  == null) return null;

		var playCard:PlayCard = null;

		switch (q.type) {
			case null:
				return null;
			case TrueFalse:
				playCard =  new cards.TrueFalseCard();
			case Multichoice:
				playCard =  new cards.MultichoiceCard();
			case Short:
				playCard =  new cards.ShortCard();
			case Description:
				playCard =   new cards.DescriptionCard();
			case Essay:
				playCard =  new cards.EssayCard();
			case Numerical:
				playCard =  new cards.NumericalCard();
			case MultiAnswers:
				playCard =  new cards.MultiAnswersCard();
			case Match:
				playCard =  new cards.MatchCard();
			case MissingWords:
				playCard =  new cards.MissingWordsCard();
		}

		playCard.questionAttempt = qA;
		playCard.setToQuestion(q);

		return playCard;

	}
	

}